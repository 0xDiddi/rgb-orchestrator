package main

import (
	"encoding/json"
    "errors"
    "fmt"
	"os"
    "path"
    "time"
)

// current policy is that only deletion of data should cause a backup, create or update not
func saveData(createBackup bool) {
	fileName := ParamFilePath + ".json"

    err := os.MkdirAll(path.Dir(fileName), 0777)
    if err != nil {
        fmt.Printf("error while creating directory for data: %v\n", err)
        return
    }

	if createBackup {
		err := os.Rename(fileName, fmt.Sprintf("%s-%s.json", ParamFilePath, time.Now().Format("06.01.02_15.04.05")))
		if err != nil {
			fmt.Printf("error while making back-up of rawData file: %v\n", err)
            // todo: re-evaluate if this should now continue without having made a backup
		}
	}

	data := IndexPageData{
		Programs:       Programs,
		CurrentProgram: CurrentProgram,
		Hardware:       KnownHardware,
		CanvasWidth:    LedCanvas.Width,
		CanvasHeight:   LedCanvas.Height,
	}

	rawData, err := json.Marshal(data)
	if err != nil {
		fmt.Printf("error while marshaling data: %v\n", err)
        return
	}

	err = os.WriteFile(fileName, rawData, 0644)
    if err != nil {
        fmt.Printf("error while writing rawData to file: %v\n", err)
    }
}

// restoreData is expected to be called only at startup, thus it calls os.Exit(1) on errors
func restoreData() {
    fileName := ParamFilePath + ".json"

    _, err := os.Stat(fileName)
    if errors.Is(err, os.ErrNotExist) {
        // no data saved yet, just do nothing
        return
    }

    rawData, err := os.ReadFile(fileName)
    if err != nil {
        fmt.Printf("error while reading rawData from file: %v\n", err)
        os.Exit(1)
    }

    data := IndexPageData{}

    err = json.Unmarshal(rawData, &data)
    if err != nil {
        fmt.Printf("error while unmarshaling data: %v\n", err)
        os.Exit(1)
    }

    Programs = data.Programs
    CurrentProgram = data.CurrentProgram
    KnownHardware = data.Hardware
    LedCanvas.Width = data.CanvasWidth
    LedCanvas.Height = data.CanvasHeight

    err = setCurrentProgram(CurrentProgram)
    if err != nil {
        fmt.Printf("error while setting current program: %v\n", err)
        os.Exit(1)
    }
}
