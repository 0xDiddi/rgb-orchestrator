package lua

import (
	"context"
	"fmt"
	"github.com/yuin/gopher-lua"
	"gitlab.com/0xdiddi/rgb-orchestrator/canvas"
	"gitlab.com/0xdiddi/rgb-orchestrator/lua/modules"
	"gitlab.com/0xdiddi/rgb-orchestrator/model"
	"sync"
	"time"
)

type WrappedState struct {
	state     *lua.LState
	tickFn    *lua.P
	tickMutex *sync.Mutex
}

const (
	CtxKeyStartTime = "start-time"
)

func (s *WrappedState) Init(cv *canvas.Canvas) {
	if s.state != nil {
		s.state.Close()
	}

	L := lua.NewState(lua.Options{
		SkipOpenLibs: true,
	})

	ctx := context.WithValue(context.Background(), modules.CtxKeyCanvas, cv)
	ctx = context.WithValue(ctx, CtxKeyStartTime, time.Now())
	L.SetContext(ctx)

	loadAllowedModules(L)

	s.state = L
	s.tickFn = nil

    if s.tickMutex == nil {
        s.tickMutex = &sync.Mutex{}
    }
}

func (s *WrappedState) LoadScript(script string) error {
	if s.state == nil {
		return fmt.Errorf("state not initialized")
	}

	if s.tickFn != nil {
		return fmt.Errorf("state already has a script loaded")
	}

	if err := s.state.DoString(script); err != nil {
		return err
	}

	tickFn := s.state.GetGlobal("tick")
	_, ok := tickFn.(*lua.LFunction)
	if !ok {
		return fmt.Errorf("script does not contain a tick function")
	}

	s.tickFn = &lua.P{
		Fn:      tickFn,
		NRet:    0,
		Protect: true,
	}

	return nil
}

func (s *WrappedState) DoTick(programs []model.Program) error {
	s.tickMutex.Lock()
	defer s.tickMutex.Unlock()

	s.updateParameterValues(programs)
	return s.internalTick()
}

func (s *WrappedState) internalTick() error {
	if s.state == nil {
		return fmt.Errorf("state not initialized")
	}

	if s.tickFn == nil {
		return fmt.Errorf("no script loaded")
	}

	return s.state.CallByParam(*s.tickFn)
}

// UpdateParameterValues does not remove old parameters.
// this _could_ cause undefined behaviour until the lua state is re-initialized
func (s *WrappedState) updateParameterValues(programs []model.Program) {
	L := s.state
	ctx := L.Context()
	mod := ctx.Value(modules.CtxKeyParameter).(*lua.LTable)

	dt := time.Since(ctx.Value(CtxKeyStartTime).(time.Time)).Seconds()
	mod.RawSetString("T", lua.LNumber(dt))

	for _, prog := range programs {
		for name, param := range prog.Parameters {
			switch param.Type {
			case model.ParamTypeRange:
				mod.RawSetString(name, lua.LNumber(param.Value[2]))
			case model.ParamTypeColor:
				value := mod.RawGetString(name)
				if value == lua.LNil {
					value = L.CreateTable(0, 3)
				}
				table := value.(*lua.LTable)
				table.RawSetString("R", lua.LNumber(param.Value[0]))
				table.RawSetString("G", lua.LNumber(param.Value[1]))
				table.RawSetString("B", lua.LNumber(param.Value[2]))

				mod.RawSetString(name, table)
			}
		}
	}
}

func loadAllowedModules(L *lua.LState) {
	// as seen in the gopher-lua readme
	for _, pair := range []struct {
		n string
		f lua.LGFunction
	}{
		{lua.LoadLibName, lua.OpenPackage},
		{lua.BaseLibName, modules.OpenCustomBase},
		{lua.TabLibName, lua.OpenTable},
		{lua.StringLibName, lua.OpenString},
		{lua.MathLibName, lua.OpenMath},
		{modules.CanvasLibName, modules.OpenCanvas},
		{modules.ParametersLibName, modules.OpenParameter},
	} {
		if err := L.CallByParam(lua.P{
			Fn:      L.NewFunction(pair.f),
			NRet:    0,
			Protect: true,
		}, lua.LString(pair.n)); err != nil {
			panic(err)
		}
	}
}
