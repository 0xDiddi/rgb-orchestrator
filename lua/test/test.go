package main

import (
	"fmt"
	"gitlab.com/0xdiddi/rgb-orchestrator/canvas"
	"gitlab.com/0xdiddi/rgb-orchestrator/lua"
)

var testScript = `
function tick()
	print("tick!")

	canvas.clear(.75, 0, 1)

	canvas.setCorner(canvas.CORNER_BL, .25, 0, 1)
	canvas.setCorner(canvas.CORNER_TL, 0, .6, 1)
	canvas.setCorner(canvas.CORNER_TR, 1, 0, .5)

	canvas.addVertex(1, 0.5, 1, .25, 1)
end
`

func main() {
	s := lua.WrappedState{}

	cv := canvas.Canvas{
		Width:          2,
		Height:         1,
		NonCornerVerts: []canvas.Vertex{},
	}

	s.Init(&cv)

	if err := s.LoadScript(testScript); err != nil {
		panic(err)
	}

	if err := s.DoTick(); err != nil {
		panic(err)
	}

	str, err := cv.ToAnsiDebugString(40, 20)
	if err != nil {
		panic(err)
	}

	fmt.Println(str)

}
