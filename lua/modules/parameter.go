package modules

import (
    "context"
    "github.com/yuin/gopher-lua"
)

const (
	ParametersLibName = "parameter"

	CtxKeyParameter = "parameter"
)

func OpenParameter(L *lua.LState) int {
    mod := L.RegisterModule(ParametersLibName, nil).(*lua.LTable)

    ctx := L.Context()
    ctx = context.WithValue(ctx, CtxKeyParameter, mod)
    L.SetContext(ctx)

    L.Push(mod)
    return 1
}
