package modules

import (
	"fmt"
	"github.com/yuin/gopher-lua"
	"strconv"
	"strings"
)

/*
 * This is a limited subset of the module defined in "github.com/yuin/gopher-lua/baselib.go"
 * It is stripped of many functions that may cause concerns in the context of executing arbitrary user code.
 */

func OpenCustomBase(L *lua.LState) int {
	global := L.Get(lua.GlobalsIndex).(*lua.LTable)
	L.SetGlobal("_G", global)
	basemod := L.RegisterModule("_G", baseFuncs)
	global.RawSetString("ipairs", L.NewClosure(baseIpairs, L.NewFunction(ipairsaux)))
	global.RawSetString("pairs", L.NewClosure(basePairs, L.NewFunction(pairsaux)))
	L.Push(basemod)
	return 1
}

var baseFuncs = map[string]lua.LGFunction{
	"assert":         baseAssert,
	"error":          baseError,
	"next":           baseNext,
	"print":          basePrint,
	"select":         baseSelect,
	"tonumber":       baseToNumber,
	"tostring":       baseToString,
	// since all allowed modules are pre-loaded, 'require' is not *required* for now
	// "require": loRequire,
}

func baseAssert(L *lua.LState) int {
	if !L.ToBool(1) {
		L.RaiseError(L.OptString(2, "assertion failed!"))
		return 0
	}
	return L.GetTop()
}

func baseError(L *lua.LState) int {
	obj := L.CheckAny(1)
	level := L.OptInt(2, 1)
	L.Error(obj, level)
	return 0
}

func ipairsaux(L *lua.LState) int {
	tb := L.CheckTable(1)
	i := L.CheckInt(2)
	i++
	v := tb.RawGetInt(i)
	if v == lua.LNil {
		return 0
	} else {
		L.Pop(1)
		L.Push(lua.LNumber(i))
		L.Push(lua.LNumber(i))
		L.Push(v)
		return 2
	}
}

func baseIpairs(L *lua.LState) int {
	tb := L.CheckTable(1)
	L.Push(L.Get(lua.UpvalueIndex(1)))
	L.Push(tb)
	L.Push(lua.LNumber(0))
	return 3
}

func baseNext(L *lua.LState) int {
	tb := L.CheckTable(1)
	index := lua.LNil
	if L.GetTop() >= 2 {
		index = L.Get(2)
	}
	key, value := tb.Next(index)
	if key == lua.LNil {
		L.Push(lua.LNil)
		return 1
	}
	L.Push(key)
	L.Push(value)
	return 2
}

func pairsaux(L *lua.LState) int {
	tb := L.CheckTable(1)
	key, value := tb.Next(L.Get(2))
	if key == lua.LNil {
		return 0
	} else {
		L.Pop(1)
		L.Push(key)
		L.Push(key)
		L.Push(value)
		return 2
	}
}

func basePairs(L *lua.LState) int {
	tb := L.CheckTable(1)
	L.Push(L.Get(lua.UpvalueIndex(1)))
	L.Push(tb)
	L.Push(lua.LNil)
	return 3
}

// todo: add a switch that when the webserver is set to prod mode, print becoems a no-op
// alternatively, use a logging framework and plug this in at debug level
func basePrint(L *lua.LState) int {
	top := L.GetTop()
	for i := 1; i <= top; i++ {
		fmt.Print(L.ToStringMeta(L.Get(i)).String())
		if i != top {
			fmt.Print("\t")
		}
	}
	fmt.Println("")
	return 0
}

func baseSelect(L *lua.LState) int {
	L.CheckTypes(1, lua.LTNumber, lua.LTString)
	switch lv := L.Get(1).(type) {
	case lua.LNumber:
		idx := int(lv)
		num := L.GetTop()
		if idx < 0 {
			idx = num + idx
		} else if idx > num {
			idx = num
		}
		if 1 > idx {
			L.ArgError(1, "index out of range")
		}
		return num - idx
		case lua.LString:
			if string(lv) != "#" {
				L.ArgError(1, "invalid string '"+string(lv)+"'")
			}
			L.Push(lua.LNumber(L.GetTop() - 1))
			return 1
	}
	return 0
}

func baseToNumber(L *lua.LState) int {
	base := L.OptInt(2, 10)
	noBase := L.Get(2) == lua.LNil

	switch lv := L.CheckAny(1).(type) {
	case lua.LNumber:
		L.Push(lv)
		case lua.LString:
			str := strings.Trim(string(lv), " \n\t")
			if strings.Index(str, ".") > -1 {
				if v, err := strconv.ParseFloat(str, lua.LNumberBit); err != nil {
					L.Push(lua.LNil)
				} else {
					L.Push(lua.LNumber(v))
				}
			} else {
				if noBase && strings.HasPrefix(strings.ToLower(str), "0x") {
					base, str = 16, str[2:] // Hex number
				}
				if v, err := strconv.ParseInt(str, base, lua.LNumberBit); err != nil {
					L.Push(lua.LNil)
				} else {
					L.Push(lua.LNumber(v))
				}
			}
			default:
				L.Push(lua.LNil)
	}
	return 1
}

func baseToString(L *lua.LState) int {
	v1 := L.CheckAny(1)
	L.Push(L.ToStringMeta(v1))
	return 1
}
