package modules

import (
	"github.com/yuin/gopher-lua"
	"gitlab.com/0xdiddi/rgb-orchestrator/canvas"
)

/*
- clear canvas
  - removes non-corner vertices
  - sets all corners to the same color
- add vertex
  - add a new vertex with position and color
- set corner color
  - set corner of a corner by index
  - has constants for indices
*/

const (
	CanvasLibName = "canvas"

	CtxKeyCanvas = "canvas"
)

func OpenCanvas(L *lua.LState) int {
	mod := L.RegisterModule(CanvasLibName, canvasFuncs).(*lua.LTable)
	mod.RawSetString("CORNER_BL", lua.LNumber(0))
	mod.RawSetString("CORNER_BR", lua.LNumber(1))
	mod.RawSetString("CORNER_TL", lua.LNumber(2))
	mod.RawSetString("CORNER_TR", lua.LNumber(3))
	L.Push(mod)
	return 1
}

var canvasFuncs = map[string]lua.LGFunction{
	"clear":     canvasClear,
	"setCorner": canvasSetCorner,
	"addVertex": canvasAddVertex,
}

/*
args: number r, number g, number b
returns: void
*/
func canvasClear(L *lua.LState) int {
	if L.GetTop() != 3 {
		L.RaiseError("invalid number of arguments in call to canvas.clear, expected 3")
		return 0
	}

	r := L.CheckNumber(1)
	g := L.CheckNumber(2)
	b := L.CheckNumber(3)

	color := canvas.Color{float64(r), float64(g), float64(b)}

	cv := L.Context().Value(CtxKeyCanvas).(*canvas.Canvas)

	cv.NonCornerVerts = cv.NonCornerVerts[:0]

	cv.BottomLeft.Color = color
	cv.BottomRight.Color = color
	cv.TopLeft.Color = color
	cv.TopRight.Color = color

	return 0
}

func canvasSetCorner(L *lua.LState) int {
	if L.GetTop() != 4 {
		L.RaiseError("invalid number of arguments in call to canvas.setCorner, expected 4")
		return 0
	}

	i := L.CheckNumber(1)
	r := L.CheckNumber(2)
	g := L.CheckNumber(3)
	b := L.CheckNumber(4)

	if i < 0 || i > 3 {
		L.RaiseError("corner index out of range, must be in range 0-3 (inclusive)")
		return 0
	}

	color := canvas.Color{float64(r), float64(g), float64(b)}

	cv := L.Context().Value(CtxKeyCanvas).(*canvas.Canvas)

	switch i {
	case 0:
		cv.BottomLeft.Color = color
	case 1:
		cv.BottomRight.Color = color
	case 2:
		cv.TopLeft.Color = color
	case 3:
		cv.TopRight.Color = color
	}

	return 0
}

func canvasAddVertex(L *lua.LState) int {
	if L.GetTop() != 5 {
		L.RaiseError("invalid number of arguments in call to canvas.addVertex, expected 5")
		return 0
	}

	x := L.CheckNumber(1)
	y := L.CheckNumber(2)
	r := L.CheckNumber(3)
	g := L.CheckNumber(4)
	b := L.CheckNumber(5)

	color := canvas.Color{float64(r), float64(g), float64(b)}

	cv := L.Context().Value(CtxKeyCanvas).(*canvas.Canvas)

	cv.NonCornerVerts = append(cv.NonCornerVerts, canvas.Vertex{
		X:     float64(x),
		Y:     float64(y),
		Color: color,
	})

	return 0
}
