package canvas_test

import (
	c "gitlab.com/0xdiddi/rgb-orchestrator/canvas"
	"testing"
)

func benchmarkSample(w, h int, b *testing.B) {
	canv := c.Canvas{
		Width:  2,
		Height: 1,

		BottomLeft:  c.Vertex{Color: c.Color{1, 0, 0}},
		BottomRight: c.Vertex{Color: c.Color{1, 1, 0}},
		TopLeft:     c.Vertex{Color: c.Color{0, 1, 1}},
		TopRight:    c.Vertex{Color: c.Color{0, 0, 1}},

		NonCornerVerts: []c.Vertex{
			{X: 1, Y: .5, Color: c.Color{1, 0, 1}},
		},
	}

	var (
		colors [][]c.Color
		err    error
	)

	for i := 0; i < b.N; i++ {
		colors, err = canv.Sample(w, h)
		if err != nil {
			b.Error(err)
		}
	}

	_ = colors
}

func BenchmarkSample_100_5(b *testing.B) {
	benchmarkSample(100, 5, b)
}

func BenchmarkSample_50_30(b *testing.B) {
	benchmarkSample(50, 30, b)
}

func BenchmarkSample_100_100(b *testing.B) {
	benchmarkSample(100, 100, b)
}

func BenchmarkSample_1000_100(b *testing.B) {
	benchmarkSample(1000, 100, b)
}
