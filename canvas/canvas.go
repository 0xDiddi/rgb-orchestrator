package canvas

import (
	"fmt"
	"github.com/fogleman/delaunay"
)

type (
	Color      [3]float64
	BaryCoords [3]float64
)

type Vertex struct {
	// Coordinates
	X, Y  float64
	Color Color
}

type Canvas struct {
	// Canvas Dimensions
	Width, Height float64

	// Corner Vertices; their coordinates are ignored and assumed to be in their repesctive corner
	BottomLeft, BottomRight, TopLeft, TopRight Vertex
	// Non-corner Vertices
	NonCornerVerts []Vertex
}

func (b BaryCoords) IsValid() bool {
	return b[0] >= 0 && b[1] >= 0 && b[2] >= 0
}

func (v Vertex) CoordsToArray() [2]float64 {
	return [2]float64{v.X, v.Y}
}

func (c Canvas) GetVertexCoordinates() [][2]float64 {
	arr := make([][2]float64, 4+len(c.NonCornerVerts))

	for i := 0; i < 4+len(c.NonCornerVerts); i++ {
		arr[i] = c.IdxToVertexCoords(i)
	}

	return arr
}

func (c Canvas) Triangulate() [][3]int {
	vertices := c.GetVertexCoordinates()

	pts := make([]delaunay.Point, len(vertices))
	for i, v := range vertices {
		pts[i] = delaunay.Point{
			X: v[0],
			Y: v[1],
		}
	}

	trig, err := delaunay.Triangulate(pts)
	if err != nil {
		panic(err)
	} else if len(trig.Triangles)%3 != 0 {
		panic(fmt.Sprintf("invalid number of vertices resulting from triangulation: %v", trig))
	}

	numTrigs := len(trig.Triangles) / 3

	trigs := make([][3]int, numTrigs)

	for i := 0; i < numTrigs; i++ {
		trigs[i] = [3]int{trig.Triangles[i*3], trig.Triangles[i*3+1], trig.Triangles[i*3+2]}
	}

	return trigs
}

func (c Canvas) IdxToVertex(i int) Vertex {
	switch i {
	case 0:
		return c.BottomLeft
	case 1:
		return c.BottomRight
	case 2:
		return c.TopLeft
	case 3:
		return c.TopRight
	default:
		return c.NonCornerVerts[i-4]
	}
}

func (c Canvas) IdxToVertexCoords(i int) [2]float64 {
	switch i {
	case 0:
		return [2]float64{0, 0}
	case 1:
		return [2]float64{c.Width, 0}
	case 2:
		return [2]float64{0, c.Height}
	case 3:
		return [2]float64{c.Width, c.Height}
	default:
		return c.NonCornerVerts[i-4].CoordsToArray()
	}
}

func (c Canvas) BarycentricCoords(verts [3]int, t [2]float64) BaryCoords {
	// as seen in https://en.wikipedia.org/wiki/Barycentric_coordinate_system#Vertex_approach

	// using 1-indexing to match the matrix on wikipedia
	p1 := c.IdxToVertexCoords(verts[0])
	p2 := c.IdxToVertexCoords(verts[1])
	p3 := c.IdxToVertexCoords(verts[2])

	l1 := p2[0]*p3[1] - p3[0]*p2[1] + t[0]*(p2[1]-p3[1]) + t[1]*(p3[0]-p2[0])
	l2 := p3[0]*p1[1] - p1[0]*p3[1] + t[0]*(p3[1]-p1[1]) + t[1]*(p1[0]-p3[0])
	l3 := p1[0]*p2[1] - p2[0]*p1[1] + t[0]*(p1[1]-p2[1]) + t[1]*(p2[0]-p1[0])

	// 1 over twice signed area
	area2 := 1 / (p1[0]*(p2[1]-p3[1]) + p2[0]*(p3[1]-p1[1]) + p3[0]*(p1[1]-p2[1]))

	l1 *= area2
	l2 *= area2
	l3 *= area2

	return BaryCoords{l1, l2, l3}
}

func (c Canvas) Sample(resW, resH int) ([][]Color, error) {
	if resW < 2 || resH < 2 {
		return nil, fmt.Errorf("sample resolution must be at least two in either direction")
	}

	triangles := c.Triangulate()
	stepX := c.Width / float64(resW-1)
	stepY := c.Height / float64(resH-1)

	trigIdx := 0

	findBary := func(x, y float64) BaryCoords {
		bary := c.BarycentricCoords(triangles[trigIdx], [2]float64{x, y})
		if bary.IsValid() {
			return bary
		}

		for i := range triangles {
			if i == trigIdx {
				continue
			}

            bary = c.BarycentricCoords(triangles[i], [2]float64{x, y})

            if bary.IsValid() {
				trigIdx = i
                return bary
			}
		}
		// this should never happen, thus I don't think we need an error return
		panic(fmt.Sprintf("sampled point (%v,%v) not in canvas area: %v\n%v\n%v", x, y, bary, triangles, c.NonCornerVerts))
	}

	res := make([][]Color, resW)

	for ix := 0; ix < resW; ix++ {
		res[ix] = make([]Color, resH)
		x := stepX * float64(ix)

		for iy := 0; iy < resH; iy++ {
			y := stepY * float64(iy)

			bary := findBary(x, y)
			trig := triangles[trigIdx]

			c0 := c.IdxToVertex(trig[0]).Color
			c1 := c.IdxToVertex(trig[1]).Color
			c2 := c.IdxToVertex(trig[2]).Color

			r0 := bary[0]*c0[0] + bary[1]*c1[0] + bary[2]*c2[0]
			r1 := bary[0]*c0[1] + bary[1]*c1[1] + bary[2]*c2[1]
			r2 := bary[0]*c0[2] + bary[1]*c1[2] + bary[2]*c2[2]

			res[ix][iy] = Color{r0, r1, r2}
		}
	}

	return res, nil
}

func (c Canvas) ToAnsiDebugString(resW, resH int) (string, error) {
	res := ""

	// internally multiplying width by 2, since characters are taller than wide
	colors, err := c.Sample(resW*2, resH)
	if err != nil {
		return "", err
	}

	for y := 0; y < resH; y++ {
		for x := 0; x < resW*2; x++ {
			clr := colors[x][y]
			res += fmt.Sprintf("\x1b[38;2;%d;%d;%dm█", int(clr[0]*255), int(clr[1]*255), int(clr[2]*255))
		}
		res += "\n"
	}

	return res, nil
}
