package main

import (
	"flag"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/0xdiddi/rgb-orchestrator/canvas"
	"gitlab.com/0xdiddi/rgb-orchestrator/lua"
	"gitlab.com/0xdiddi/rgb-orchestrator/model"
	"golang.org/x/time/rate"
	"time"
)

var (
	Programs       []model.Program
	CurrentProgram int
	LuaState       lua.WrappedState

	LedCanvas              canvas.Canvas
	SampleResX, SampleResY int // these will be configurable at some point
	CurrentSample          [][]canvas.Color

	Ticker        *time.Ticker
	currentPeriod time.Duration

	KnownHardware model.HwList

	ParamFilePath    string
	ParamHostAddress string
)

type IndexPageData struct {
	Programs                  []model.Program
	CurrentProgram            int
	Hardware                  model.HwList
	CanvasWidth, CanvasHeight float64
}

func main() {
	flag.StringVar(&ParamFilePath, "data", "./userdata/data", "The name of the user data file, without extension")
	flag.StringVar(&ParamHostAddress, "addr", ":8080", "The address for the web server to listen on")
	flag.Parse()

	router := gin.New()
	router.Use(
		gin.LoggerWithConfig(gin.LoggerConfig{
			SkipPaths: []string{"/hw-data"},
		}),
		gin.Recovery(),
	)

	// passing nil here will never return an error
	_ = router.SetTrustedProxies(nil)

	configureRoutes(router)

	Programs = make([]model.Program, 0)
	CurrentProgram = -1

	// canvas size will be configurable at some point
	LedCanvas = canvas.Canvas{
		Width:          2,
		Height:         1,
		NonCornerVerts: []canvas.Vertex{},
	}

	SampleResX, SampleResY = 600, 300

	LuaState = lua.WrappedState{}
	Ticker = time.NewTicker(time.Second)
	go tickerFunc(Ticker.C)

	KnownHardware = make(model.HwList)

	restoreData()

	err := router.Run(ParamHostAddress)
	if err != nil {
		fmt.Printf("error while running server: %v\n", err)
	}
}

func setCurrentProgram(idx int) error {
	CurrentProgram = idx

	if idx < 0 || len(Programs) == 0 {
		// leave the program running; not ideal but works for now
		return nil
	}

	LuaState.Init(&LedCanvas)

	prog := Programs[CurrentProgram]

	err := LuaState.LoadScript(prog.LuaScript)

	currentPeriod = time.Second / time.Duration(prog.TickFrequency)
	Ticker.Reset(currentPeriod)

	lim := rate.Limit(prog.TickFrequency)
	for _, mod := range KnownHardware {
		mod.SetLimit(lim)
	}

	return err
}

func tickerFunc(tick <-chan time.Time) {
	for range tick {
		if CurrentProgram < 0 {
			continue
		}

		err := LuaState.DoTick(Programs)
		if err != nil {
			panic(err)
		}

		CurrentSample, _ = LedCanvas.Sample(SampleResX, SampleResY)

		for _, mod := range KnownHardware {
			if mod.PushMode {
				mod.SendData(currentPeriod, CurrentSample, LedCanvas.Width, LedCanvas.Height)
			}
		}
	}

	fmt.Println("ticker function ended, this should only happen at shutdown")
}

func configureRoutes(router *gin.Engine) {
	router.Static("/css", "web/static")

	router.LoadHTMLGlob("web/template/*")

	router.GET("/", func(c *gin.Context) {
		data := IndexPageData{
			Programs:       Programs,
			CurrentProgram: CurrentProgram,
			Hardware:       KnownHardware,
			CanvasWidth:    LedCanvas.Width,
			CanvasHeight:   LedCanvas.Height,
		}

		c.HTML(200, "index.html", data)
	})

	setupEndpointsForProgram(router)

	setupEndpointsForParameter(router)

	setupEndpointsForCode(router)

	setupEndpointsForHardware(router)
}
