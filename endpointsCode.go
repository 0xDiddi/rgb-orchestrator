package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"image"
	"image/color"
	"image/png"
	"net/http"
	"strconv"
)

func setupEndpointsForCode(router *gin.Engine) {
	router.PUT("/code", func(c *gin.Context) {
		if len(Programs) == 0 {
			c.Status(http.StatusNotFound)
			return
		}

		params := make(map[string]string)

		err := c.Bind(&params)
		if err != nil {
			// c.Bind() already handles error response
			fmt.Println("error while binding parameter request", err)
			return
		}

		code, ok := params["code"]
		if !ok {
			c.JSON(http.StatusBadRequest, gin.H{"error": "no code provided"})
			return
		}

		Programs[CurrentProgram].LuaScript = code

		// used here as a wrapper to re-run the program
		err = setCurrentProgram(CurrentProgram)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"msg": err.Error(), "err": err})
			return
		}

        saveData(false)

		c.Status(http.StatusNoContent)
	})

	router.GET("/canvas", func(c *gin.Context) {
		var (
			w = 200
			h = 100
		)

		queryParam := c.Query("w")
		if queryParam != "" {
			var err error
			w, err = strconv.Atoi(queryParam)
			if err != nil {
				c.JSON(http.StatusBadRequest, gin.H{"error": err})
				return
			}
		}

		queryParam = c.Query("h")
		if queryParam != "" {
			var err error
			h, err = strconv.Atoi(queryParam)
			if err != nil {
				c.JSON(http.StatusBadRequest, gin.H{"error": err})
				return
			}
		}

		img := image.NewRGBA(image.Rect(0, 0, w, h))

        // do an immediate tick so that the canvas is up-to-date from any update that may have just been pushed
        err := LuaState.DoTick(Programs)
        if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"msg": err.Error(), "err": err})
            return
        }

		pixels, err := LedCanvas.Sample(w, h)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"msg": err.Error(), "err": err})
			return
		}

		for x := 0; x < w; x++ {
			for y := 0; y < h; y++ {
				img.Set(x, h-y-1, color.RGBA{
					R: uint8(pixels[x][y][0] * 255),
					G: uint8(pixels[x][y][1] * 255),
					B: uint8(pixels[x][y][2] * 255),
					A: 255,
				})
			}
		}

		for _, mod := range KnownHardware {
			for _, strip := range mod.Strips {
				pts := strip.GetSamplePoints(LedCanvas.Width, LedCanvas.Height, w, h)

				for _, point := range pts {
					x := point[0]
					y := h - point[1]

					pix := img.At(x, y).(color.RGBA)

					// invert color at sample points
					img.Set(x, y, color.RGBA{
						R: 255 - pix.R,
						G: 255 - pix.G,
						B: 255 - pix.B,
						A: 255,
					})
				}
			}
		}

		err = png.Encode(c.Writer, img)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"msg": err.Error(), "err": err})
			return
		}

		c.Header("Content-Type", "image/png")
		c.Status(http.StatusOK)
	})
}
