package model

import "math"

type ParamType string

const (
	// ParamTypeRange means values are min, max and current value of slider
	ParamTypeRange ParamType = "range"
	// ParamTypeColor means value is the chosen color
	ParamTypeColor ParamType = "color"
)

type Parameter struct {
	Type  ParamType
	Value [3]float64
}

type Program struct {
	Parameters    map[string]Parameter
	LuaScript     string
	Name          string
	TickFrequency int
}

func (p *Parameter) Sanitize() {
	switch p.Type {
	case ParamTypeRange:
		// ensure max value is at least 1 above minimum (since step size is always 1)
		p.Value[1] = math.Max(p.Value[0]+1, p.Value[1])
		// ensure current value is between min and max values
		p.Value[2] = math.Max(p.Value[0], math.Min(p.Value[1], p.Value[2]))
	case ParamTypeColor:
		// ensure all color channels are within 0-1 range
		p.Value[0] = math.Max(0, math.Min(1, p.Value[0]))
		p.Value[1] = math.Max(0, math.Min(1, p.Value[1]))
		p.Value[2] = math.Max(0, math.Min(1, p.Value[2]))
	}
}
