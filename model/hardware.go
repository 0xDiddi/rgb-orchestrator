package model

import (
	"bytes"
	"context"
	"fmt"
	"gitlab.com/0xdiddi/rgb-orchestrator/canvas"
	"golang.org/x/time/rate"
	"net"
	"time"
)

// HwList maps a modules identifier to the module data
type HwList map[string]*HwModule

type HwModule struct {
	Address  string
	Strips   []LedStrip
	PushMode bool
	conn     net.Conn
	limiter  *rate.Limiter
}

type LedStrip struct {
	Start, End Point
	NumLEDs    int
}

type Point struct {
	X, Y float64
}

func (strip LedStrip) GetSamplePoints(canvasWidth, canvasHeight float64, outputWidth, outputHeight int) [][2]int {
	// add one to LED count so that the samples can be centered between points
	// divide by canvas size to normalize, so that later we can multiply by output size
	stepX := (strip.End.X - strip.Start.X) / canvasWidth / float64(strip.NumLEDs+1)
	stepY := (strip.End.Y - strip.Start.Y) / canvasHeight / float64(strip.NumLEDs+1)

	// normalize strip points to canvas size
	nX0 := strip.Start.X / canvasWidth
	nY0 := strip.Start.Y / canvasHeight

	pts := make([][2]int, strip.NumLEDs)

	for i := 0; i < strip.NumLEDs; i++ {
		// add one more step to center samples between points
		fX := nX0 + stepX*float64(i+1)
		fY := nY0 + stepY*float64(i+1)

		iX := int(float64(outputWidth) * fX)
		iY := int(float64(outputHeight) * fY)

		pts[i] = [2]int{iX, iY}
	}

	return pts
}

func (mod *HwModule) SetLimit(lim rate.Limit) {
	if mod.limiter == nil {
		mod.limiter = rate.NewLimiter(lim, 1)
	} else {
		mod.limiter.SetLimit(lim)
	}
}

func (mod *HwModule) Wait(c context.Context) error {
	return mod.limiter.Wait(c)
}

func (mod *HwModule) GetData(colors [][]canvas.Color, cW, cH float64) *bytes.Buffer {
	buf := &bytes.Buffer{}
	sW := len(colors)
	sH := len(colors[0])

	// iterate back to front over both, so that the LED closest to the controller is last
	// that way the data can be directly forwarded to the shift registers
	for idxS := len(mod.Strips) - 1; idxS >= 0; idxS-- {
		pts := mod.Strips[idxS].GetSamplePoints(cW, cH, sW, sH)
		for idxP := len(pts) - 1; idxP >= 0; idxP-- {
			pt := pts[idxP]
			pix := colors[pt[0]][pt[1]]

			buf.WriteByte(byte(pix[0] * 255))
			buf.WriteByte(byte(pix[1] * 255))
			buf.WriteByte(byte(pix[2] * 255))
		}
	}

	return buf
}

func (mod *HwModule) connect() (err error) {
	ip := net.ParseIP(mod.Address)
	addr := &net.TCPAddr{
		IP:   ip,
		Port: 5678,
	}

	mod.conn, err = net.DialTCP("tcp", nil, addr)
	if err != nil {
		// somehow despite printing as "<nil>", it _isn't_ nil so we need to explicitly set it
		mod.conn = nil
	}
	return err
}

func (mod *HwModule) SendData(limit time.Duration, colors [][]canvas.Color, cW, cH float64) {
	if mod.conn == nil {
		err := mod.connect()
		if err != nil {
			// we'll just try again next tick
			return
		}
	}

	buf := mod.GetData(colors, cW, cH)

	err := mod.conn.SetWriteDeadline(time.Now().Add(limit / 2))
	if err != nil {
		fmt.Println("error while setting deadline", err)
	}

	n, err := mod.conn.Write(buf.Bytes())
	if err != nil {
		_ = mod.conn.Close()
		mod.conn = nil
	} else if n != buf.Len() {
		fmt.Println("incomplete write to module", mod.Address)
	}
}
