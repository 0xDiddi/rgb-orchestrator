package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
    "gitlab.com/0xdiddi/rgb-orchestrator/model"
    "net/http"
	"strconv"
	"strings"
)

func setupEndpointsForParameter(router *gin.Engine) {
	router.POST("/parameter", func(c *gin.Context) {
		var params struct {
			Type string
			Name string
		}

		err := c.Bind(&params)
		if err != nil {
			// c.Bind() already handles error response
			fmt.Println("error while binding parameter request", err)
			return
		}

		prog := &Programs[CurrentProgram]

		var (
			_type        = model.ParamType(params.Type)
			defaultValue [3]float64
		)

		_, alreadyExists := prog.Parameters[params.Name]
		if alreadyExists {
			c.JSON(http.StatusConflict, gin.H{"error": "conflicting parameter name", "conflicting value": params.Name})
			return
		}

		switch _type {
		case model.ParamTypeRange:
			defaultValue = [3]float64{0, 10, 1}
            case model.ParamTypeColor:
			defaultValue = [3]float64{.75, 0, 1}
		}

        prog.Parameters[params.Name] = model.Parameter{
			Type:  _type,
			Value: defaultValue,
		}

        saveData(false)

		c.Status(http.StatusCreated)
	})

	router.DELETE("parameter", func(c *gin.Context) {
		if len(Programs) == 0 {
			c.Status(http.StatusNotFound)
			return
		}

		name := c.Query("name")
		if name == "" {
			c.Status(http.StatusBadRequest)
			return
		}

		prog := &Programs[CurrentProgram]

		delete(prog.Parameters, name)

        saveData(true)

		c.Status(http.StatusNoContent)
	})

	router.POST("/set-parameters", func(c *gin.Context) {
		if len(Programs) == 0 {
			c.Status(http.StatusNotFound)
			return
		}

		params := make(map[string]string)

		err := c.Bind(&params)
		if err != nil {
			// c.Bind() already handles error response
			fmt.Println("error while binding parameter request", err)
			return
		}

		for k, v := range params {
			parts := strings.Split(k, "-")
			if len(parts) != 4 || parts[0] != "param" || parts[1] != "value" {
				c.JSON(http.StatusBadRequest, gin.H{"error": "invalid parameter key", "faulty value": k})
				return
			}

			paramName := parts[2]
			paramIdx, err := strconv.Atoi(parts[3])

			if err != nil {
				c.JSON(http.StatusBadRequest, gin.H{"error": "invalid parameter value index", "faulty value": parts[3], "cause": err})
				return
			} else if paramIdx < 0 || paramIdx > 2 {
				c.JSON(http.StatusBadRequest, gin.H{"error": "parameter value index out of range", "faulty value": paramIdx})
				return
			}

			param := Programs[CurrentProgram].Parameters[paramName]

			param.Value[paramIdx], err = strconv.ParseFloat(v, 64)
			if err != nil {
				c.JSON(http.StatusBadRequest, gin.H{"error": "invalid parameter value", "faulty value": v, "cause": err})
				return
			}

			param.Sanitize()

			Programs[CurrentProgram].Parameters[paramName] = param
		}

        saveData(false)
        
		c.Status(http.StatusNoContent)
	})
}
