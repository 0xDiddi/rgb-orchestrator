#ifndef MODE
#define MODE

#include "common.h"

int tcpServer() {
  static WiFiServer server(5678);
  static bool initServer = false;

  static WiFiClient client;

  if (!initServer) {
    server.begin();
    initServer = true;
  }

  if (!client) {
    client = server.available();
  }

  if (!client.connected()) {
    pixels.setPixelColor(0, pixels.Color(255, 0, 0));
    pixels.show();
    return 0;
  }

  int totalRead = 0;

  while (client.available() > 0) {
    int read = client.read(&buf[offset], BUF_SIZE-offset);
    offset += read;
    totalRead += read;
    yield();
  }

  return totalRead;
}

void mode_loop() {
  int read = tcpServer();

  if (read == 0) {
    delay(2);
  }

  if (offset >= BUF_SIZE) {
    for(int i=0; i<NUM_LED; i++) {
      pixels.setPixelColor(i, pixels.Color(buf[i*3], buf[i*3+1], buf[i*3+2]));
    }

    pixels.show();
    offset = 0;
  }
}

#endif
