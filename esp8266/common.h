#pragma once

#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <Adafruit_NeoPixel.h>

/*
This must provide: "const char* ssid", "const char* pwd", "const char* orchestratorUrl", "#define NUM_LED <int>"
- orchestratorUrl must be of the form "http://<orchestrator host/ip>:<port>/hw-module?hwid=<id of this module>"
- NUM_LED must be the (integer) number of LEDs

If mode_pull is used, "const char* pullUrl" must also be provided
- pullUrl must be of the form "http://<orchestrator host/ip>:<port>/hw-data?hwid=<id of this module>"
*/
#include "connection_constants.h"

Adafruit_NeoPixel pixels(NUM_LED, D1);

#define BUF_SIZE NUM_LED * 3
char buf[BUF_SIZE];
int offset;
