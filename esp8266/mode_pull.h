#ifndef MODE
#define MODE

#include "common.h"

void mode_loop() {
  static WiFiClient client;
  static HTTPClient http;

  http.begin(client, pullUrl);

  int respCode = http.GET();
  if (respCode != 200) {
    if (respCode < 0) {
      Serial.println(http.errorToString(respCode));
      pixels.setPixelColor(0, pixels.Color(255, 0, 0));
      pixels.show();
    } else {
      Serial.println(respCode);
      pixels.setPixelColor(0, pixels.Color(255, 100, 0));
      pixels.show();
    }
    http.end();
    return;
  }

  while (offset < BUF_SIZE) {
    int read = client.read(&buf[offset], BUF_SIZE-offset);
    offset += read;
    yield();
  }

  for(int i=0; i<NUM_LED; i++) {
    pixels.setPixelColor(i, pixels.Color(buf[i*3], buf[i*3+1], buf[i*3+2]));
  }
  pixels.show();

  offset = 0;
  http.end();
}

#endif
