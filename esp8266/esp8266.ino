#include "common.h"

/*
Include one or the other, not both.

mode_push: will accept a tcp connection at port 5678 and receive data from there
mode_pull: will make http requests to pull data, "const char* pullUrl" must be defined in "connection_constants.h"
*/
// #include "mode_push.h"
#include "mode_pull.h"

void setup() {
  Serial.begin(115200);

  pixels.begin();
  pixels.setPixelColor(0, pixels.Color(0, 93, 255));
  pixels.show();

  WiFi.begin(ssid, pwd);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  pixels.setPixelColor(0, pixels.Color(255, 0, 160));
  pixels.show();

  Serial.println("\nconnected!");
  Serial.println(WiFi.localIP());

  registerWithOrchestrator();
}

void loop() {
  mode_loop();
}

void registerWithOrchestrator() {
  WiFiClient client;
  HTTPClient http;

  http.begin(client, orchestratorUrl);

  int responseCode = http.POST("");

  http.end();

  Serial.print("registration resp: ");
  Serial.println(responseCode);
}
