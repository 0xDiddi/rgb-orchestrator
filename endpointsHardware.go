package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/0xdiddi/rgb-orchestrator/model"
	"net/http"
	"strconv"
)

func setupEndpointsForHardware(router *gin.Engine) {
	router.POST("/hw-module", func(c *gin.Context) {
		remote := c.RemoteIP()
		identifier := c.Query("hwid")

		if identifier == "" {
			c.JSON(http.StatusBadRequest, gin.H{"error": "invalid hardware identifier provided"})
			return
		}

		module, alreadyExists := KnownHardware[identifier]
		if alreadyExists {
			if module.Address == remote {
				// act idempotent if the same module calls multiple times, not accounting for dhcp
				c.Status(http.StatusNoContent)
				return
			}
			c.JSON(http.StatusConflict, gin.H{"error": "conflicting hardware identifier", "conflicting value": identifier})
			return
		}

		KnownHardware[identifier] = &model.HwModule{
			Address: remote,
			Strips:  []model.LedStrip{},
		}

		saveData(false)

		c.Status(http.StatusCreated)
	})

	router.DELETE("/hw-module", func(c *gin.Context) {
		identifier := c.Query("hwid")
		if identifier == "" {
			c.JSON(http.StatusBadRequest, gin.H{"error": "invalid hardware identifier provided"})
			return
		}

		_, found := KnownHardware[identifier]
		if !found {
			c.JSON(http.StatusNotFound, gin.H{"error": "hardware identifier not found", "conflicting value": identifier})
			return
		}

		delete(KnownHardware, identifier)

		saveData(true)

		c.Status(http.StatusNoContent)
	})

	router.POST("/led-strip", func(c *gin.Context) {
		identifier := c.Query("hwid")
		if identifier == "" {
			c.JSON(http.StatusBadRequest, gin.H{"error": "invalid hardware identifier provided"})
			return
		}

		module, found := KnownHardware[identifier]
		if !found {
			c.JSON(http.StatusNotFound, gin.H{"error": "hardware identifier not found", "conflicting value": identifier})
			return
		}

		module.Strips = append(module.Strips, model.LedStrip{
			Start:   model.Point{.25, .25},
			End:     model.Point{.75, .75},
			NumLEDs: 1,
		})

		KnownHardware[identifier] = module

		saveData(false)

		c.Status(http.StatusNoContent)
	})

	router.DELETE("/led-strip", func(c *gin.Context) {
		identifier := c.Query("hwid")
		if identifier == "" {
			c.JSON(http.StatusBadRequest, gin.H{"error": "invalid hardware identifier provided"})
			return
		}

		module, found := KnownHardware[identifier]
		if !found {
			c.JSON(http.StatusNotFound, gin.H{"error": "hardware identifier not found", "conflicting value": identifier})
			return
		}

		idx, err := strconv.Atoi(c.Query("idx"))
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err})
			return
		}

		if idx < 0 || idx >= len(module.Strips) {
			c.JSON(http.StatusNotFound, gin.H{"error": "selected index not found"})
			return
		}

		module.Strips = append(module.Strips[:idx], module.Strips[idx+1:]...)

		saveData(true)

		c.Status(http.StatusNoContent)
	})

	router.PUT("/led-strip", func(c *gin.Context) {
		identifier := c.Query("hwid")
		if identifier == "" {
			c.JSON(http.StatusBadRequest, gin.H{"error": "invalid hardware identifier provided"})
			return
		}

		module, found := KnownHardware[identifier]
		if !found {
			c.JSON(http.StatusNotFound, gin.H{"error": "hardware identifier not found", "conflicting value": identifier})
			return
		}

		idx, err := strconv.Atoi(c.Query("idx"))
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err})
			return
		}

		if idx < 0 || idx >= len(module.Strips) {
			c.JSON(http.StatusNotFound, gin.H{"error": "selected index not found"})
			return
		}

		var data struct {
			X0, Y0, X1, Y1 float64
			NumLeds        int
		}

		err = c.Bind(&data)
		if err != nil {
			// c.Bind() already handles error response
			fmt.Println("error while binding request body", err)
			return
		}

		module.Strips[idx] = model.LedStrip{
			Start:   model.Point{data.X0, data.Y0},
			End:     model.Point{data.X1, data.Y1},
			NumLEDs: data.NumLeds,
		}

		saveData(false)

		c.Status(http.StatusNoContent)
	})

	router.GET("/hw-data", func(c *gin.Context) {
		identifier := c.Query("hwid")
		if identifier == "" {
			c.JSON(http.StatusBadRequest, gin.H{"error": "invalid hardware identifier provided"})
			return
		}

		module, found := KnownHardware[identifier]
		if !found {
			c.JSON(http.StatusNotFound, gin.H{"error": "hardware identifier not found", "conflicting value": identifier})
			return
		}

		err := module.Wait(c)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "failed to limit", "err": err})
			fmt.Println("failed to wait", err)
			return
		}

		buf := module.GetData(CurrentSample, LedCanvas.Width, LedCanvas.Height)

		_, err = buf.WriteTo(c.Writer)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "error while writing response", "err": err})
			fmt.Println("error writing response", err)
			return
		}

		c.Header("Content-Type", "application/octet-stream")
		c.Status(200)
	})
}
