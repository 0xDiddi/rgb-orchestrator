package main

import (
	"github.com/gin-gonic/gin"
    "gitlab.com/0xdiddi/rgb-orchestrator/model"
    "net/http"
	"strconv"
)

func setupEndpointsForProgram(router *gin.Engine) {
	router.POST("/program", func(c *gin.Context) {
		name := c.Query("name")
		if name == "" {
			c.JSON(http.StatusBadRequest, gin.H{"error": "name must be provided"})
			return
		}

		Programs = append(Programs, model.Program{
			Name:          name,
			Parameters:    make(map[string]model.Parameter),
			TickFrequency: 25,
			LuaScript: `
function tick()
    canvas.clear(.75, 0, 1)
end
`,
		})

		err := setCurrentProgram(len(Programs) - 1)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"msg": err.Error(), "err": err})
			return
		}

        saveData(false)

		c.Status(http.StatusCreated)
	})

	router.DELETE("/program", func(c *gin.Context) {
		if len(Programs) == 0 {
			c.Status(http.StatusNotFound)
			return
		}

		idxStr := c.Query("idx")
		var idx int
		if idxStr == "" {
			idx = CurrentProgram
		} else {
			var err error
			idx, err = strconv.Atoi(idxStr)
			if err != nil {
				c.JSON(http.StatusBadRequest, gin.H{"error": err})
				return
			}
		}

		if idx < 0 || idx >= len(Programs) {
			c.JSON(http.StatusNotFound, gin.H{"error": "selected index not found"})
			return
		}

		Programs = append(Programs[:idx], Programs[idx+1:]...)

		if CurrentProgram >= len(Programs) {
			err := setCurrentProgram(len(Programs) - 1)
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"msg": err.Error(), "err": err})
				return
			}
		}

        saveData(true)

		c.Status(http.StatusNoContent)
	})

	router.PUT("/current-program", func(c *gin.Context) {
		if len(Programs) == 0 {
			c.Status(http.StatusNotFound)
			return
		}

		idx, err := strconv.Atoi(c.Query("idx"))
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err})
			return
		}

		if idx < 0 || idx >= len(Programs) {
			c.JSON(http.StatusNotFound, gin.H{"error": "selected index not found"})
			return
		}

		err = setCurrentProgram(idx)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"msg": err.Error(), "err": err})
			return
		}

        saveData(false)
        
		c.Status(http.StatusNoContent)
	})
}
